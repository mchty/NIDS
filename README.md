# **Network Intrusion Detection Dataset (NIDS)**

**Siber güvenlik**, hem güvenliğin hem de rakiplerin birbirini alt etmeye çalıştığı, yeni saldırılar, bu saldırılara karşı yeni savunma yolları ve yine bu savunmaları alt etmenin yeni yollarını bulduğu bir silahlanma yarışıdır. Bu durum, yeni, gerçekçi siber güvenlik veri kümelerine yönelik sürekli bir ihtiyaç yaratmaktadır. 

Bu çalışmada, gerçek hayattaki bir mimariden gelen ağ trafiğinde makine öğrenimi tabanlı **izinsiz giriş tespit yöntemleri** kullanmanın etkilerini tanıtmaktadır. Bu çalışmanın ana katkısı, gerçek dünyadan, akademik bir ağdan gelen bir veri kümesidir. Gerçek hayattaki trafik toplandı ve bir dizi saldırı gerçekleştirildikten sonra bir veri seti toplandı. Ağ veri şeması Netflow v9 biçimindedir ve 44 benzersiz özellik ve her çerçeveyi açıklayan bir etiket içerir.

**İzinsiz ağ giriş tespit sistemleri (NIDS),** ağdaki tüm cihazlardan gelen trafiği incelemek için ağ içinde planlanmış bir noktada kurulur. Tüm alt ağda geçen trafiği gözlemler ve alt ağlarda geçirilen trafiği bilinen saldırılar koleksiyonuyla eşleştirir. Bir saldırı belirlendiğinde veya anormal bir davranış gözlemlendiğinde, yöneticiye **uyarı** gönderilebilir.

